#ifndef SORT_H_INCLUDED
#define SORT_H_INCLUDED

#include <cmath>

namespace takg
{
    /*
        1. Bubble Sort
        2. Selection Sort
        3. Insertion Sort
        4. Shell Sort
        5. Merge Sort
        6. Quick Sort
        7. Heap Sort
        8. Radix Sort
    */
    /*
    The below function performs sorting using bubble sort algorithm
    */
    template <typename T>
    bool bubbleSort(T* array, const int size)
    {
        T temp; // used for swap

        // outer loop loops for maxsize - 1
        // this is because array of size = 1 is already sorted
        for (int nLoopOuter = 0 ; nLoopOuter < (size - 1) ; ++nLoopOuter)
        {
            bool bSortCompleted = true;
            // inner loop shall sort the elements from 0 to (maxsize-1)
            // but again in each outer loop the max element gets sorted
            // so max limit check is (size-1) - nLoopOuter
            for (int nLoopInner = 0 ; nLoopInner < (size - 1) - nLoopOuter ; ++nLoopInner)
            {
                // check for the immediate next two elements
                // we are relying on operator < to be defined
                if ( array[nLoopInner+1] < array[nLoopInner] )
                {
                    // swap the two elements
                    temp = array[nLoopInner];
                    array[nLoopInner] = array[nLoopInner+1];
                    array[nLoopInner+1] = temp;
                    bSortCompleted = false;
                }
            }
            // if the input data structure is already sorted before n-1 iterations
            // then stop sorting
            if (bSortCompleted)
            {
                break;
            }
        }
        return true;
    } /* end of bubble sort */

    /*
    The below function performs sorting using selection sort algorithm
    */
    template <typename T>
    bool selectionSort(T* array, const int size)
    {
        T temp; // used for swap
        int index=0;

        // outer loop loops for size - 1
        // this is because array of size = 1 is already sorted
        for (int nLoopOuter = 0 ; nLoopOuter < (size - 1) ; ++nLoopOuter)
        {
            // inner loop shall sort the elements from 0 to (size-1)
            // but again in each outer loop the min element get sorted out
            // so starting from nLoopOuter
            index = nLoopOuter;
            temp = array[index];

            for (int nLoopInner = nLoopOuter+1; nLoopInner <size; ++nLoopInner)
            {
                // check for the immediate next two elements
                // we are relying on operator < to be defined
                if ( array[nLoopInner] < temp )
                {
                    // swap the two elements
                    temp = array[nLoopInner];
                    index = nLoopInner;
                }
                // perform the swap
                if ( index != nLoopOuter)
                {
                    array[index] = array[nLoopOuter];
                    array[nLoopOuter] = temp;
                }
            }
        }

        return true;
    } /* end of selection sort */

    /*
    The below function performs sorting using insertion sort algorithm
    */
    template <typename T>
    bool insertionSort(T* array, const int size)
    {
        T temp; // used for swap

        // outer loop loops for maxsize - 1
        // this is because array of size = 1 is already sorted
        for (int nLoopOuter = 1 ; nLoopOuter < size ; ++nLoopOuter)
        {
            // inner loop shall sort the elements from nLoopOuter to 0
            temp = array[nLoopOuter];
            int nLoopInner = 0;
            for (nLoopInner = nLoopOuter ; nLoopInner >= 1 ; --nLoopInner)
            {
                // check for the immediate next two elements
                // we are relying on operator < to be defined
                if ( temp < array[nLoopInner-1] )
                {
                    // swap the two elements
                    array[nLoopInner] = array[nLoopInner-1];
                }
                else
                {
                    break;
                }
            }

            // check for if swap is required
            if (nLoopInner != nLoopOuter)
            {
                array[nLoopInner] = temp;
            }
        }
        return true;
    } /* end of insertion sort */

    /*
    The below function performs sorting using shell sort algorithm
    */
    template <typename T>
    bool shellSort(T* array, const int size)
    {
        T temp; // used for swap

        // create gaps based on size
        int gapLength = static_cast<int>(log(size)+1);
        int *gapArray = new int[gapLength];
        for ( int nLoop = 0 ; nLoop < gapLength; ++nLoop)
        {
            gapArray[nLoop] = static_cast<int>(pow(2,(gapLength-1-nLoop)));
        }

        for ( int nLoopGap = 0 ; nLoopGap < gapLength ; ++nLoopGap)
        {
            const int gap = gapArray[nLoopGap];

            // outer loop loops for maxsize - 1
            // this is because array of size = 1 is already sorted
            for (int nLoopOuter = gap ; nLoopOuter < size ; ++nLoopOuter)
            {
                // inner loop shall sort the elements from nLoopOuter to 0
                temp = array[nLoopOuter];
                int nLoopInner = 0;
                for (nLoopInner = nLoopOuter ; nLoopInner >= gap ; nLoopInner -= gap)
                {
                    // check for the immediate next two elements
                    // we are relying on operator < to be defined
                    if ( temp < array[nLoopInner-gap] )
                    {
                        // swap the two elements
                        array[nLoopInner] = array[nLoopInner-gap];
                    }
                    else
                    {
                        break;
                    }
                }

                // check for if swap is required
                if (nLoopInner != nLoopOuter)
                {
                    array[nLoopInner] = temp;
                }
            }
        }

        // clear the memory created on help
        delete[] gapArray;

        return true;
    } /* end of shell sort */

    /*
    The below function performs sorting using merge sort algorithm
    */
    template <typename T>
    bool mergeSort(T* array, const int left, const int right)
    {
        // split the array till the size is 1
        // size == one is a sorted array

        if (left < right )
        {
            int left1 = left;
            int right1 = (left+right)/2;
            int left2 = right1+1;
            int right2 = right;

            mergeSort(array, left1, right1);
            mergeSort(array, left2, right2);

            int index1 = left1;
            int index2 = left2;

            T* tempArray = new T[right-left+1];
            int index3 = 0;

            while ( (index1 <= right1) && (index2 <= right2) )
            {
                if ( array[index1] < array[index2] )
                {
                    tempArray[index3++] = array[index1++];
                }
                else
                {
                    tempArray[index3++] = array[index2++];
                }
            }

            // either index1 > right1 or index2 > right2
            while (index1 <= right1)
            {
                tempArray[index3++] = array[index1++];
            }

            while (index2 <= right2)
            {
                tempArray[index3++] = array[index2++];
            }
            // populate the data back on array

            index1=0;
            for ( index3 = left; index3 <= right ; ++index3)
                array[index3] = tempArray[index1++];

            // clear the memory allocated
            delete[] tempArray;
        }

        return true;
    } /* end of merge sort */
} /* end of namespance takg */

#endif // SORT_H_INCLUDED
