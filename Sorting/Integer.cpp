#include "Integer.hpp"

using namespace takg;

int takg::Integer::assignmentCount=0;
int takg::Integer::comparisionCount=0;

const bool PRINT = false;

std::string getOrder(int count, int size)
{
    std::stringstream order;

    if ( count < log(size))
    {
        order <<"log(n)="<< log(size);
    }
    else if ( count < size)
    {
        order <<"n="<< size;
    }
    else if ( count < size*log(size))
    {
        order <<"n*log(n)="<< size*log(size);
    }
    else if ( count < size*size)
    {
        order <<"n^2="<< size*size;
    }
    else
    {
        order <<"n^3="<<size*size*size;
    }

    return order.str();
}

// This function prints the statistics
void Integer::print(const std::string& sortAlgoName, Integer* array, const int size)
{
    std::stringstream output;

    const std::string delimiter = "\t";

    static bool printOnce = true;
    if ( printOnce )
    {
        // print the header once
        output << std::endl << "Sorting Algorithm"  << delimiter
                            << "Total Comparisions" << delimiter
                            << "Total Assignments"         << delimiter
                            << "Total Size"         << delimiter
                            << "Order of Comp" << delimiter
                            << "Order of Assignment" << delimiter;
        printOnce = false;
    }

    // print the sorting algo and it's stats
    output << std::endl << sortAlgoName << delimiter
                        << comparisionCount << delimiter
                        << assignmentCount << delimiter
                        << size << delimiter;

    output << getOrder(comparisionCount, size) << delimiter << getOrder(assignmentCount, size);

    std::cout << output.str();

    printArray("Output Array", array, size);
    // reset the values;
    reset();
}

void Integer::printArray(const std::string& name, Integer* array, const int size)
{
    if (!PRINT)
        return;

    std::cout << std::endl << name << "::";

    for ( int nLoop = 0 ; nLoop < size ; ++nLoop)
    {
        std::cout << array[nLoop].value << ",";
    }
}
