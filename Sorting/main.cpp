#include <iostream>
#include "Sort.hpp"
#include "Integer.hpp"


template <class Functor>
void runAllSortingAlgorithms(Functor func)
{
    const int MAX_SIZE = 100;
    takg::Integer array[MAX_SIZE] = {0};

    // bubble sort
    takg::bubbleSort(func(array,MAX_SIZE), MAX_SIZE);
    takg::Integer::print("bubble sort", array, MAX_SIZE);

    // selection sort
    takg::selectionSort(func(array,MAX_SIZE), MAX_SIZE);
    takg::Integer::print("selection sort", array, MAX_SIZE);

    // insertion sort
    takg::insertionSort(func(array,MAX_SIZE), MAX_SIZE);
    takg::Integer::print("insertion sort", array, MAX_SIZE);

    // shell sort
    takg::shellSort(func(array,MAX_SIZE), MAX_SIZE);
    takg::Integer::print("shell sort", array, MAX_SIZE);

    // merge sort
    takg::mergeSort(func(array,MAX_SIZE), 0, MAX_SIZE-1);
    takg::Integer::print("merge sort", array, MAX_SIZE);
    takg::Integer::printArray("testing",array, MAX_SIZE);

    return;
}

int main()
{
    std::cout << "Welcome to the complexity analysis of sorting algorithms program!!!" << std::endl;

    /* Run for reverse sorted array */
    runAllSortingAlgorithms(takg::IntegerReverseArray());
    /* Run for best case scenario i.e. sorted array */
    runAllSortingAlgorithms(takg::IntegerSortedArray());

    return 0;
}

