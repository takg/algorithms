#ifndef INTEGER_HPP_INCLUDED
#define INTEGER_HPP_INCLUDED

#include <iostream>
#include <sstream>
#include <cmath>

namespace takg
{
    class Integer
    {
    public:
        Integer() {value = 0;}

        Integer(const int value) {this->value = value;}

        bool operator< (const Integer& rhs)
        {
            ++comparisionCount;
            return (value < rhs.value);
        }

        Integer& operator= (const Integer& rhs)
        {
            ++assignmentCount;
            value = rhs.value;
            return *this;
        }

        Integer& operator= (const int rhs)
        {
            value = rhs;
            return *this;
        }

        static void print(const std::string& in_sortAlgo, Integer* array, const int in_MaxSize);
        static void printArray(const std::string& in_sortAlgo, Integer* array, const int in_MaxSize);

        static void reset()
        {
            assignmentCount = 0;
            comparisionCount = 0;
        }

    private:
        static int comparisionCount;
        static int assignmentCount;
        int value;
    }; /* end of class Integer */

    /* get an array in the reverse order */
    struct IntegerReverseArray
    {
        Integer* operator()(Integer* array, const int size)
        {
            // reverse
            for (int nLoop = 0; nLoop < size; ++nLoop)
                array[nLoop] = size - nLoop;

            Integer::printArray("Input Array",array,size);
            return array;
        }
    };

    /* get an array in the sorted order */
    struct IntegerSortedArray
    {
        Integer* operator()(Integer* array, const int size)
        {
            // sorted
            for (int nLoop = 1; nLoop <= size; ++nLoop)
                array[nLoop-1] = nLoop;

            Integer::printArray("Input Array",array,size);

            return array;
        }
    };
} /* end of namespace takg */

#endif // INTEGER_HPP_INCLUDED
